%dw 1.0

%var exchangeRate = 1.35

%function processByType1(input)
input match {
	:array -> "input is an array of size $(sizeOf input)",
	:object  -> (type:"object") ++ input,
	default -> "input $(input) is of type " ++ typeOf input
}



%function matchString(aString)
aString match {
  lax: "LAX" -> lower lax,
  theDest when ( (lower theDest) == "sfo") -> 
    upper theDest, 

  theCode: /([a-z]){4}(\d){4}/ -> upper theCode[0],

  default -> "default $(aString)"
}

%function processByType2(input)
input match {
  //First filter arrays and objects
  theArray is :array  -> "input is an array of size $(sizeOf theArray)",

  :object -> "input is an object",
	
  //Simple literal type test
  :number -> matchNumber(input),

  aString is :string -> matchString(aString),

  default -> "input $(input) is of type " ++  (typeOf input)
}

%function processByType(input)
input match {
  //First filter arrays and objects
  theArray is :array  -> theArray,

  :object -> input,
  
  //Simple literal type test
  :number -> formatNumber(input),

  aString is :string -> matchString(aString),

  default -> "input $(input) is of type " ++  (typeOf input)
}



%function matchNumber(aNumber)
aNumber match {
  numberInput: 11 -> "The number goes to 11" 
    ++ numberInput as :string {format: "###"} ,

  aDecimal when (ceil aDecimal) - aDecimal > 0 -> 
  aDecimal as :string {format: "###"} as :number,

  default -> "input $(aNumber) is of type " ++ typeOf aNumber
}

%function formatNumber(value)
using (strValue = value as :string) 
( strValue match 
  {
    /^\d+$/ -> value as :string {format: "###"},
    /^\d*\.\d*$/ -> value as :string {format: "##0.00"},
    default -> value
  }
)


%function matchString(aString)
aString match {
  lax: "LAX" -> lower lax,
  theDest when ( (lower theDest) == "sfo") -> 
    upper theDest, 

  theCode: /([a-z]){4}(\d){4}/ -> upper theCode[0],

  default -> "default $(aString)"
}



---

//Provide external names for variables and functions defined in the header
{
  exchangeRate: exchangeRate,
  processByType: processByType,
  formatString: matchString,
  formatNumber: matchNumber
}

