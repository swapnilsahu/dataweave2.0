%dw 2.0

var exchangeRate = 1.35

fun combineScatterGatherResults( theInput ) =
( 
	flatten(theInput..flights) //orderBy $.price
)

fun buildXmlObject( objectArray, parentTag ) =
{ 
   (dw::core::Strings::pluralize(parentTag))  : {( 
      objectArray map (element) -> {
         (parentTag): element
      }
   )}
}


fun buildJsonObject( objectArray: Array<Object>, parentTag: String ) =
{( 
   objectArray map (element, index) -> {
      ( parentTag ++ index ): element
   }
)}

fun shuffleObject( anObject, shuffledIndices ) =
do {
    var pluckedKeys = dw::core::Objects::nameSet(anObject)
    ---
    {( 
        shuffledIndices map (shuffledIndex, origIndex) ->
	 { (pluckedKeys[shuffledIndex]) : anObject[shuffledIndex] }
    )}
} 

fun formatKeys( anyInput ) =
 anyInput match {
 	case is Array -> anyInput map formatKeys($)
 	case is Object -> anyInput mapObject (value, key ) ->  
 		{ (lower(key) ) : formatKeys(value) }
 	case is String -> anyInput 
 	case is Number -> anyInput
 	else -> anyInput
 }


fun formatDataStructure( anyInput ) =
 anyInput match {
 	case is Array -> anyInput map formatDataStructure($)
 	case is Object -> anyInput mapObject (value, key ) ->  
 		{ (upper(key) ) : formatDataStructure(value) }
 	case is String -> lower( anyInput ) 
 	case is Number -> anyInput as String {format: "+#,##0.00;-#"}
 	else -> anyInput
 }



